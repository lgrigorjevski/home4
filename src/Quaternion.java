import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




//https://introcs.cs.princeton.edu/java/32class/Quaternion.java.html
/** Quaternions. Basic operations. */
public class Quaternion {

   public static final double DELTA = 0.000001;
   private double r, i, j, k;


   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.r = a;
      this.i = b;
      this.j = c;
      this.k = d;



   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {

      return r;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {

      return i;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {

      return j;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {

      return k;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      return r+this.num2str(i)+"i"+this.num2str(j)+"j"+this.num2str(k)+"k";
      //-1.0-2.0i-3.0j-5k
   }

   private String num2str(double n){
      if(n>=0)
         return "+"+n;
      return String.valueOf(n);
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */

   //https://habrahabr.ru/post/260773/
   //http://www.quizful.net/post/Java-RegExp
   // http://www.javenue.info/post/43
   //http://proglang.su/java/strings-split
   //http://www.regular-expressions.info/floatingpoint.html
   public static Quaternion valueOf (String s) {

      // String min = s.replace("-", "+-");
      // String plus = min.replace("++", "+");
      Pattern p = Pattern.compile("[-+]?[0-9]+\\.?[0-9]*[-+][0-9]+\\.?[0-9]*i[-+][0-9]+\\.?[0-9]*j[-+][0-9]+\\.?[0-9]*k");
      Matcher m = p.matcher(s);
       /*System.out.println("Avaldise kontroll " + s +
                  (m.matches() ? " lubatud." : " keelatud. Kasutada saab ainult selline formaat: a+bi+cj+dk"));*/

      //System.out.println(m.groupCount());
      //if (m.groupCount() != 4) throw new IllegalArgumentException("Avaldis: " + s + "");
      if(m.matches() == false) throw new IllegalArgumentException("Avaldise kontroll! Vigane avaldis: " + s + " Kasutada saab ainult selline formaat: a+bi+cj+dk");


      //System.out.println(s);
//-1.0+-2.0i+-3.0j+-5k
      String rString[] = s.split("(?=[+-])", 2);
      if(rString[0].toString().isEmpty())throw new IllegalArgumentException("Avaldis: " + s + "Ei ole r arv");
      double r = Double.parseDouble(rString[0]);

      //System.out.println(r + "r");

   /*System.out.println(r + "r");
   System.out.println(rString[0]);
   System.out.println(rString[1]);
   System.out.println(Arrays.toString(rString));*/
      //-2.0i+-3.0j+-5k
      String iString[] = rString[1].split("i(?=[+-])", 2);
      if(iString[0].toString().isEmpty())throw new IllegalArgumentException("Avaldis: " + s + " Ei ole i arv");
      double i = Double.valueOf(iString[0]);
   /*System.out.println(i);
   System.out.println(iString[0]);
   System.out.println(iString[1]);
   System.out.println(Arrays.toString(iString));*/

      String jString[] = iString[1].split("j(?=[+-])", 2);
      if(jString[0].toString().isEmpty())throw new IllegalArgumentException("Avaldis: " + s + " Ei ole j arv");
      double j = Double.valueOf(jString[0]);
      /*System.out.println(j);
      System.out.println(jString[0]);
      System.out.println(jString[1]);
      System.out.println(Arrays.toString(jString));*/

      String kString[] = jString[1].split("k", 2);
      if(kString[0].toString().isEmpty())throw new IllegalArgumentException("Avaldis: " + s + " Ei ole j arv");
      double k = Double.valueOf(kString[0]);
      /*System.out.println(r + "ddd");
      System.out.println(kString[0]);
      System.out.println(kString[1]);
      System.out.println(Arrays.toString(kString));*/

      return new Quaternion(r, i, j, k);

   }



   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */


   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(r, i, j, k);
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {

      if(Math.abs(r) < DELTA && Math.abs(i) < DELTA && Math.abs(j) < DELTA && Math.abs(k) < DELTA){
         return true;
      }

      return false;
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */

   public Quaternion conjugate() {
      return new Quaternion(r, -i, -j, -k);
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {

      return new Quaternion(-r, -i , -j , -k );
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {

      return new Quaternion(r + q.r, i + q.i, j + q.j, k + q.k);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   //https://introcs.cs.princeton.edu/java/32class/Quaternion.java.html
   //a2 = q.r, b2 = q.i, c2 = q.j, d2 = q.k
   public Quaternion times (Quaternion q) {
      return new Quaternion(
              r * q.r - i * q.i - j * q.j - k * q.k, //a
              r * q.i + q.r * i + j * q.k - k * q.j, //b
              r * q.j - i * q.k + j * q.r + k * q.i, //c
              r * q.k + i * q.j - j * q.i + k * q.r //d
      );
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {

      return new Quaternion(this.r * r, i * r, j * r, k * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   //https://introcs.cs.princeton.edu/java/32class/Quaternion.java.html
   public Quaternion inverse() {
      if (isZero()) throw new RuntimeException("Jagamine nulliga ei ole lubatud.");
      double all = r * r + i * i + j * j + k * k;
      double r = this.r / all;
      double i = (-this.i) / all;
      double j = (-this.j)/all;
      double k = (-this.k)/all;

      return new Quaternion(r,i,j,k);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {

      return new Quaternion(r - q.r, i - q.i, j - q.j, k - q.k);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   //https://introcs.cs.princeton.edu/java/32class/Quaternion.java.html
   public Quaternion divideByRight (Quaternion q) {
      if (isZero()) throw new RuntimeException("Jagamine nulliga ei ole lubatud.");
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (isZero()) throw new RuntimeException("Jagamine nulliga ei ole lubatud.");
      return q.inverse().times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if(qo instanceof Quaternion
              && minus((Quaternion)qo).isZero()){

         return true;
      }

      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {

      Quaternion p = this;
      Quaternion uus = p.times(q.conjugate()).plus(q.times(p.conjugate()));
      return new Quaternion(uus.r / 2., uus.i / 2., uus.j / 2., uus.k / 2.);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {

      return toString().hashCode();
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {

      return Math.sqrt(r * r + i * i + j * j + k * k);
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      System.out.println(Arrays.toString("1-34k+3j".split("(?=[+-])", 2)));

      Quaternion arv1 = new Quaternion (1., -2., -3., -5.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);

      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate().toString());
      System.out.println ("opposite: " + arv1.opposite().toString());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
              + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -3., 5.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res.toString());
      System.out.println ("times: " + arv1.times (arv2).toString());
      System.out.println ("minus: " + arv1.minus (arv2).toString());
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse().toString());
      System.out.println ("divideByRight: " + arv1.divideByRight(arv2).toString());
      System.out.println ("divideByLeft: " + arv1.divideByLeft(arv2).toString());
      System.out.println ("dotMult: " + arv1.dotMult(arv2));
      Quaternion.valueOf("1-2i-3j-4k");


   }
}
// end of file
